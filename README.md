# data_science_study

데이터사이언스 역량 개발 과정과 모든 학습 자료 관리

## 제로베이스 네카라쿠배 데이터사이언스 1기 자료

- 기간 : 2021년 09월 13일 ~ 진행중

<br>
<hr>

### 프로젝트 진행

- Python을 이용한 서울시에 위치한 이디야, 스타벅스 매장 위치 상관관계 분석
```
# Chrome Driver 다운로드 : https://chromedriver.chromium.org/downloads
```
[![파이썬을 이용한 서울시 이디야, 스타벅스 매장 위치 상관관계 분석](https://img.youtube.com/vi/qobe7k1CmGc/0.jpg)](https://youtu.be/qobe7k1CmGc)

- Python의 fbprophet을 이용한 시계열 데이터 분석

```
- Prophet 자료 : https://predictor-ver1.tistory.com/4
- Microsoft C++ build tools : https://visualstudio.microsoft.com/ko/visual-cpp-build-tools/
- StackOverflow fbprophet 종속성 관련 질문 : https://stackoverflow.com/questions/53178281/installing-fbprophet-python-on-windows-10
```
[![파이썬을 이용한 KIA 주식 데이터 시계열 분석 연습](https://img.youtube.com/vi/RsC5NnqDFxg/0.jpg)](https://youtu.be/RsC5NnqDFxg)

- 파이썬을 이용한 대한민국 코로나 방역 대응 평가 분석

[![파이썬을 이용한 대한민국 코로나 방역 대응 평가 분석](https://img.youtube.com/vi/objsuVB8wzs/0.jpg)](https://youtu.be/objsuVB8wzs)

<br>
<hr>

## 제로베이스 데이터사이언스 온라인 완주반(고급) 자료

- 기간 : 상시 학습 진행

- 참고 : 
    1. 데이터사이언스스쿨 : https://datascienceschool.net/
