import random
import time
import sys

import requests


AWS_GATEWAY_INVOKE_URL = "https://l91io5lg1a.execute-api.ap-northeast-2.amazonaws.com/prod/product"
PRODUCT_CATEGORIES = ["artes", "cool_stuff", "automotivo", "perfumaria", "pcs"]

def get_random_user():
    return int(random.random() * 100) + 1

def get_random_product_category():
    return random.choice(PRODUCT_CATEGORIES)

def random_sleep():
    time.sleep(random.random() * 10)
    return True


try:
    while True:
        res = requests.post(AWS_GATEWAY_INVOKE_URL, json={
            "user": get_random_user(),
            "product_category": get_random_product_category()
        })
        print(res)

        random_sleep()

except KeyboardInterrupt:
    print("stopped")
    sys.exit(1)
                       